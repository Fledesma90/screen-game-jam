using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCinematic : MonoBehaviour
{
    public GameObject cinematicCamera;
    public GameObject vignette1;
    int vignetteNumber=1;
    public GameObject vignette2;
    public GameObject vignette3;
    public float speedToMoveCamera;
    public float timeToChangeCamera;
    public GameObject fade;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CameraMovement());
    }

    // Update is called once per frame
    void Update()
    {
        FirstCinematicCamera();
    }

    void FirstCinematicCamera()
    {
        switch (vignetteNumber)
        {
            case 1:
                cinematicCamera.transform.position = Vector3.MoveTowards(cinematicCamera.transform.position,
    vignette1.transform.position, speedToMoveCamera * Time.deltaTime);
                break;
            case 2:
                cinematicCamera.transform.position = Vector3.MoveTowards(cinematicCamera.transform.position,
    vignette2.transform.position, speedToMoveCamera * Time.deltaTime);
                break;
            case 3:
                cinematicCamera.transform.position = Vector3.MoveTowards(cinematicCamera.transform.position,
    vignette3.transform.position, speedToMoveCamera * Time.deltaTime);
                break;
        }
    }

    IEnumerator CameraMovement()
    {
        vignetteNumber = 1;
        Debug.Log(vignetteNumber);
        yield return new WaitForSeconds(timeToChangeCamera);
        vignetteNumber=2;
        Debug.Log(vignetteNumber);
        yield return new WaitForSeconds(timeToChangeCamera);
        vignetteNumber = 3;
        Debug.Log(vignetteNumber);
        yield return new WaitForSeconds(timeToChangeCamera);
        fade.SetActive(true);
        StartCoroutine(fade.GetComponent<Fade>().FadeOut());
       
    }
}
