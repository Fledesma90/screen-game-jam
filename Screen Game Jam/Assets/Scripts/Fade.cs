using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    //referencia al canvasgroup, con este componente haremos el fade
    CanvasGroup canvasGroup;
    [Header("Tiempo de fade")]
    public float time;
    // Start is called before the first frame update
    void Start()
    {
        //recogemos la referencia al canvasgroup
        canvasGroup = GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn());
    }
    /// <summary>
    /// Mientras que el alpha del canvas group sea menor que 0, al alpha
    /// del canvasgroup se le restará Time.deltatime dividido por time
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeIn()
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime / time;
            yield return null;
        }
        if (canvasGroup.alpha == 0)
        {
            gameObject.SetActive(false);
        }
    }

    public IEnumerator FadeOut()
    {
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime / time;
            yield return null;
        }
    }
}
